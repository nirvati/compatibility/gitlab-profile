# Nirvati Compatibility

This is a collection of various Nirvati add-ons and apps that can be used to enhance compatibility with other server management systems. The goal is to reimplement the functionality of non-free systems, or fork and re-use parts of other FOSS systems.

In general, apps running through a compatibility layer may not support certain advanced Nirvati features, but the goal here is to get as fas as possible.
